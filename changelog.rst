==========
Change Log
==========

API and Python module versioning information.

List of features and available or target versions, organized by component.
(See :doc:`components`). Link to documentation in :doc:`reference` section and
appropriate content in :doc:`userguide` or :doc:`developerguide` if available. Should be accompanied by appropriate annotations elsewhere in the docs. I.e. See `Sphinx docs. <http://www.sphinx-doc.org/en/stable/markup/para.html>`_

.. note::
    If global semantic versioning is used, every feature should
    support feature-based compatibility checks by dependent code,
    such as by being able to report its own last-changed version.

.. rubric:: 0.2

.. rubric:: 0.1

Runner supports simulation from TPR file.

.. rubric:: 0.0

Design docs and architecture requirements.
